import torch
import torch.nn as nn
import torch.nn.functional as F

import config


class SobelConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1,
                 padding=0, dilation=1, groups=1, bias=True, requires_grad=True):
        """
        convolution layer with fixed kernels for directional derivatives
        :param in_channels: (int) Number of channels in the input image
        :param out_channels: (int) Number of channels produced by the layer
        :param kernel_size: (int) Size of the convolving kernel
        :param stride: (int) Stride of the convolution
        :param padding: (int) Zero-padding added to both sides of the input
        :param dilation:(int) Spacing between kernel elements
        :param groups: (int) Number of blocked connections from input channels to output channels
        :param bias: (bool) If True, adds a learnable bias to the output.
        :param requires_grad: (bool)  if the parameter requires gradient (if it is trainable or fixed)
        """
        assert kernel_size % 2 == 1, 'SobelConv2d\'s kernel_size must be odd.'
        assert out_channels % 4 == 0, 'SobelConv2d\'s out_channels must be a multiple of 4.'
        assert out_channels % groups == 0, 'SobelConv2d\'s out_channels must be a multiple of groups.'

        super(SobelConv2d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.groups = groups


        # In non-trainable case, it turns into normal Sobel operator with fixed weight and no bias.
        self.bias = bias if requires_grad else False

        if self.bias:
            self.bias = nn.Parameter(torch.zeros(size=(out_channels,), dtype=torch.float32), requires_grad=True)
        else:
            self.bias = None


        sobel_mat = torch.zeros(size=(out_channels, int(in_channels / groups), kernel_size, kernel_size))
        # Initialize the Sobel kernal
        kernel_mid = kernel_size // 2
        for idx in range(out_channels):
            if idx % 4 == 0:
                sobel_mat[idx, :, 0, :] = -1
                sobel_mat[idx, :, 0, kernel_mid] = -2
                sobel_mat[idx, :, -1, :] = 1
                sobel_mat[idx, :, -1, kernel_mid] = 2
            elif idx % 4 == 1:
                sobel_mat[idx, :, :, 0] = -1
                sobel_mat[idx, :, kernel_mid, 0] = -2
                sobel_mat[idx, :, :, -1] = 1
                sobel_mat[idx, :, kernel_mid, -1] = 2
            elif idx % 4 == 2:
                sobel_mat[idx, :, 0, 0] = -2
                for i in range(0, kernel_mid + 1):
                    sobel_mat[idx, :, kernel_mid - i, i] = -1
                    sobel_mat[idx, :, kernel_size - 1 - i, kernel_mid + i] = 1
                sobel_mat[idx, :, -1, -1] = 2
            else:
                sobel_mat[idx, :, -1, 0] = -2
                for i in range(0, kernel_mid + 1):
                    sobel_mat[idx, :, kernel_mid + i, i] = -1
                    sobel_mat[idx, :, i, kernel_mid + i] = 1
                sobel_mat[idx, :, 0, -1] = 2
        self.sobel_weight = nn.Parameter(sobel_mat, requires_grad=False)
        # Define the trainable sobel factor
        if requires_grad:
            self.sobel_factor = nn.Parameter(torch.ones(size=(out_channels, 1, 1, 1), dtype=torch.float32),
                                             requires_grad=True)
        else:
            self.sobel_factor = nn.Parameter(torch.ones(size=(out_channels, 1, 1, 1), dtype=torch.float32),
                                             requires_grad=False)

    def forward(self, x):
        """
        apply sobel filter layer
        :param x: (Tensor[B,in_ch,H,W]), input image
        :return: (Tensor[B,out_ch_ch,H,W]), output channels
        """
        sobel_weight = self.sobel_weight * self.sobel_factor
        out = F.conv2d(x, sobel_weight, self.bias, self.stride, self.padding, self.dilation, self.groups)
        return out


class EDCNN(nn.Module):

    def __init__(self, in_ch=1, out_ch=32, sobel_ch=32):
        """
        main smoothing model
        :param in_channels: (int) Number of channels in the input image
        :param out_channels: (int) Number of channels produced by the CNN layer
        :param sobel_ch: (int) Number of channels produced sobel layer
        """
        super(EDCNN, self).__init__()
        if config.double_input:
            in_ch = 2
        self.conv_sobel = SobelConv2d(1, sobel_ch, kernel_size=3, stride=1, padding=1, bias=True)
        self.conv_p1 = nn.Conv2d(in_ch + sobel_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p1 = nn.BatchNorm2d(out_ch)
        self.conv_f1 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f1 = nn.BatchNorm2d(out_ch)

        self.conv_p2 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p2 = nn.BatchNorm2d(out_ch)
        self.conv_f2 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f2 = nn.BatchNorm2d(out_ch)

        self.conv_p3 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p3 = nn.BatchNorm2d(out_ch)
        self.conv_f3 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f3 = nn.BatchNorm2d(out_ch)

        self.conv_p4 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p4 = nn.BatchNorm2d(out_ch)
        self.conv_f4 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f4 = nn.BatchNorm2d(out_ch)

        self.conv_p5 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p5 = nn.BatchNorm2d(out_ch)
        self.conv_f5 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f5 = nn.BatchNorm2d(out_ch)

        self.conv_p6 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p6 = nn.BatchNorm2d(out_ch)
        self.conv_f6 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f6 = nn.BatchNorm2d(out_ch)

        self.conv_p7 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p7 = nn.BatchNorm2d(out_ch)
        self.conv_f7 = nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1)
        #self.bn_f7 = nn.BatchNorm2d(out_ch)

        self.conv_p8 = nn.Conv2d(in_ch + sobel_ch + out_ch, out_ch, kernel_size=1, stride=1, padding=0)
        #self.bn_p8 = nn.BatchNorm2d(out_ch)
        self.conv_f8 = nn.Conv2d(out_ch, 1, kernel_size=3, stride=1, padding=1)
        #self.bn_f8 = nn.BatchNorm2d(in_ch)
        self.dropout = nn.Dropout(0.5)

        self.relu = nn.LeakyReLU()

    def forward(self, x, x2=None):
        """
        apply smoothing model
        :param x: (tensor[B,in_ch,H,W]), input image
        :return: (tensor[B,in_ch,H,W]), predicted smooth image
        """
        out_00 = self.conv_sobel(x)
        if config.double_input:
            out_0 = torch.cat((x, x2, out_00), dim=-3)
        else:
            out_0 = torch.cat((x, out_00), dim=-3)

        out_1 = self.relu(self.conv_p1(out_0))
        out_1 = self.relu(self.conv_f1(out_1))
        out_1 = torch.cat((out_0, out_1), dim=-3)

        out_2 = self.relu(self.conv_p2(out_1))
        out_2 = self.relu(self.conv_f2(out_2))
        out_2 = torch.cat((out_0, out_2), dim=-3)

        out_3 = self.relu(self.conv_p3(out_2))
        out_3 = self.relu(self.conv_f3(out_3))
        out_3 = torch.cat((out_0, out_3), dim=-3)

        out_4 = self.relu(self.conv_p4(out_3))
        out_4 = self.relu(self.conv_f4(out_4))
        out_4 = torch.cat((out_0, out_4), dim=-3)

        out_5 = self.relu(self.conv_p5(out_4))
        out_5 = self.relu(self.conv_f5(out_5))
        out_5 = torch.cat((out_0, out_5), dim=-3)

        out_6 = self.relu(self.conv_p6(out_5))
        out_6 = self.relu(self.conv_f6(out_6))
        out_6 = torch.cat((out_0, out_6), dim=-3)

        out_7 = self.relu(self.conv_p7(out_6))
        out_7 = self.relu(self.conv_f7(out_7))
        out_7 = torch.cat((out_0, out_7), dim=-3)

        out_8 = self.relu(self.conv_p8(out_7))
        out_8 = self.conv_f8(out_8)

        out = self.relu(x + out_8)
        return out
