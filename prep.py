# Prepre 2D data for NLM training including INPUT, ASIR and ASIR+NLM

import os
import argparse
import numpy as np
import pydicom
from pathlib import Path
import glob
import config
import pathlib
from os import path
from solver import transform_2dnpy_2_dicom_with_meta

def save_dataset_z(args, data_set):
    if not os.path.exists(args.np_save_path):
        os.makedirs(args.np_save_path, exist_ok=True)
        print('Create path : {}'.format(args.np_save_path))

    # loop patients
    for p_ind, (patient_id, patient_paths) in enumerate(data_set.items()):
        print('processing patient {} folder (patient {}/{})...'.format(os.path.basename(patient_id), p_ind+1, len(data_set)))

        # loop types (original/ASIR/NLM)
        for io, path_ in patient_paths.items():
            print('reading {}...'.format(io), end='\r')
            medio_array = load_scan(Path(path_))
            print('done reading {}'.format(io))

            # loop slices
            for pi in range(medio_array.shape[2]):
                print('saving slice {}/{} from {}'.format(pi+1,medio_array.shape[2], io), end='\r')
                patient_name = Path(patient_id).name
                f = normalize_(medio_array[:, :, pi], config.norm_range_min, config.norm_range_max)
                f_name = '{}_{}_{}.npy'.format(patient_name, str(pi).zfill(6), io)
                np.save(os.path.join(args.np_save_path, f_name), f)
            print('saved all slices!                  ')

        print(' ')


def normalize_(image, MIN_B=-1024.0, MAX_B=3072.0):
    """
    normalization of pixel range
    :param image: np array, image to normalize
    :param MIN_B: float, minimum of range of normalization
    :param MAX_B: float, maximum of range of normalization
    :return: np array, normalized image
    """
    image = (image - MIN_B) / (MAX_B - MIN_B)
    return image


def load_scan(path):
    """
    load a dicom folder
    :param path: str, path to dicom folder
    :return: 3d np array of CT
    """
    slices = [pydicom.dcmread(os.path.join(path, s), force=True) for s in sorted(os.listdir(path))]
    slices.sort(key=lambda x: int(x.InstanceNumber))
    image = np.stack([s.pixel_array for s in slices],axis=2)
    image = np.flip(image, axis=2)
    image = image.astype(np.int16)
    image[image == -2000] = 0
    for slice_number in range(len(slices)):
        intercept = slices[slice_number].RescaleIntercept
        slope = slices[slice_number].RescaleSlope
        if slope != 1:
            image[:, :, slice_number] = slope * image[:, :, slice_number].astype(np.float64)
            image[:, :, slice_number] = image[:, :, slice_number].astype(np.int16)
        image[:, :, slice_number] += np.int16(intercept)
    return np.array(image, dtype=np.int16)





def get_ct_path_from_dataset_NLM(dataset_path, single_path):
    """
    creates a nested dictionary of paths divided by patient name, and divided again by type (original/ASIR/NLM)
    :param dataset_path: str, path to folder containing patients folder
    :param single_path: str, name of a single patient if testing. if training on multiple patients, None.
    :return: dictionary
    """
    data_set = {}
    if single_path:
        case_lst = dataset_path.glob(single_path)
    else:
        case_lst = dataset_path.glob('*')
    # create patients dict
    for case_path in case_lst:
        if not case_path.is_dir():
            continue
        dir_paths = os.listdir(case_path)
        case_dict = {}
        # create types dict
        for dir_path in dir_paths:
            if 'asir' in dir_path.lower():
                case_dict["ASIR"] = str(case_path / dir_path)
            elif 'nlm' in dir_path.lower():
                case_dict["NLM"] = str(case_path / dir_path)
            elif 'smooth' in dir_path.lower():
                if config.double_input:
                    case_dict["SMOOTH"] = str(case_path / dir_path)
            else:
                case_dict['original'] = str(case_path / dir_path)

        # nest types dict in patients dict
        data_set[str(case_path)] = case_dict
    return data_set


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--data_path', type=str, default=config.input_data_path)
    parser.add_argument('--np_save_path', type=str, default=config.np_save_path)
    parser.add_argument('--single_case', type=str, default=config.single_case)
    parser.add_argument('--create_NLM', type=str, default=config.create_nlm)

    args = parser.parse_args()
    # create
    if args.create_NLM:
        lst = glob.glob(args.data_path+'/*/*ASIR*')
        # for patient in lst:
        for patient in lst:
            arr = load_scan(patient)
            new_dir = pathlib.Path(path.join(path.dirname(patient), 'NLM'))
            print(new_dir)
            transform_2dnpy_2_dicom_with_meta(patient, arr, new_dir, 0, 'NLM', apply_nlm_bool=True, load=False)

    dataset_path = Path(args.data_path)
    data_set = get_ct_path_from_dataset_NLM(dataset_path, args.single_case)

    save_dataset_z(args, data_set)



