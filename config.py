# default parameters

# training parameters:
# added option to auto generate output paths:
generate_path = False
# choose if test or train with model:
mode = 'train'
# path of numpy data to train on/to test:
saved_path = "/projects/Arineta/data/dataset_NLM_z_test/" if mode == 'test' else "/Users/omerc/Documents/arineta_data/Arineta upload_np/"
# path to save training weights/test predictions:
save_path = "/Users/omerc/Documents/arineta_data/trace/"
# path of saved weights for transfer training/testing:
transfer_weights = "double_persep_weights_230000iter.ckpt"
# iteration of saved weights:
test_iters = 230000
# name of test patient to avoid using in training:
test_patient = 'Friendship_ST_000418'
# original dicom path to use metadata:
dicom_path = "/projects/Arineta/data/original/"
# description of new dicom file:
series_description = "2D ED-CNN computed- double persep loss 0.1"
# weather to apply post processing NLM:
apply_nlm = False

# normalization range
norm_range_min =-1024.0
norm_range_max = 3072.0
# reconstruction range
trunc_min = -1024.0
trunc_max = 3072.0

# training parameters
patch_n = 20  # number of patches per slice for training:
patch_size = 64  # height and width of patch
batch_size = 4  # number of slices in a train iteration mini batch:
num_epochs = 2500  # number of times running through the whole data
print_iters = 20  # print results every print_iters iterations
decay_iters = 3000  # decrease learning rate every decay_iters iterations
save_iters = 5000  # save checkpoint every save_iters iterations
init_with_transfer = False  # if True, initiate weights with existing pretrained weights before training
lr = 1e-5  # learning rate for training

# gpu definitions
device = 'cuda:0'  # gpu to use
num_workers = 1  # how many processors in parallel

# prep parameters
auto_prep = False  # if True, dicom input is transformed to numpy within main code
input_data_path = "/projects/Arineta/data/NLM denoised data for RSIP/"  # dicom input to be transformed to numpy
np_save_path = '/projects/Arineta/data/test_dataset test'  # where to save numpy input

single_case = None  # if within the input folder there is more than one case, and we only want to transform one case.
# should be set as name of this case (folder name)

create_nlm = False  # whether to add nlm to folder
double_input = False
