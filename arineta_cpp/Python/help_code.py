import shutil
from pathlib import Path
import numpy as np
from pydicom.uid import generate_uid
from pydicom import dcmread
import os
import argparse


def load_scan(path):
    """
    load a dicom folder
    :param path: str, path to dicom folder
    :return: 3d np array of CT
    """
    slices = [dcmread(os.path.join(path, s), force=True) for s in sorted(os.listdir(path))]
    slices.sort(key=lambda x: int(x.InstanceNumber))
    image = np.stack([s.pixel_array for s in slices],axis=2)
    image = np.flip(image, axis=2)
    image = image.astype(np.int16)
    image[image == -2000] = 0
    for slice_number in range(len(slices)):
        intercept = slices[slice_number].RescaleIntercept
        slope = slices[slice_number].RescaleSlope
        if slope != 1:
            image[:, :, slice_number] = slope * image[:, :, slice_number].astype(np.float64)
            image[:, :, slice_number] = image[:, :, slice_number].astype(np.int16)
        image[:, :, slice_number] += np.int16(intercept)
    return np.array(image, dtype=np.int16)


def save_raw_from_dicom(dicom_path, raw_path):
    """
    saves a raw data file from dicom
    :param dicom_path: path to folder containing dicom slice
    :param raw_path: path to save raw data
    """
    image = load_scan(dicom_path)
    print(raw_path)
    image.tofile(raw_path)


def transform_npy_2_dicom_with_meta(original_dicom_path, raw_path, output_dicom_path, series_number_add=10,
                                      description='smooth'):
    """
    transform np data to dicom using a specific dicom file metadata
    :param original_dicom_path: str, path of original dicom folder to take metadata from
    :param npy_path: str, path of folder containing np data to transform
    :param output_dicom_path: str, path to save created dicom files
    :param series_number_add: int, random number to add to avoid collision
    :param description: str, description of new dicom file
    :return: saves dicom file in dir
    """
    # create path
    if Path(output_dicom_path).is_dir():
        shutil.rmtree(Path(output_dicom_path))
    Path(output_dicom_path).mkdir(exist_ok=True, parents=True)


    # load array
    a = np.fromfile(raw_path, dtype="int16").reshape((512, 512, 280))
    arr = a + 1024
    N = arr.shape[2]
    print('\n')


    # process dicom
    series_instance_uid = generate_uid(prefix=None)

    for j, original_filename in enumerate(sorted(Path(original_dicom_path).glob('*'))):
        print('processing file {}/{}'.format(j + 1, N), end="\r")

        # prep metadata
        ds = dcmread(original_filename)
        ds.SeriesNumber = str(int(ds.SeriesNumber + series_number_add))
        ds_description_list = ds.SeriesDescription.split(',')
        ds_description_list.insert(2, description)
        ds_description = ','.join(ds_description_list)
        ds.SeriesDescription = ds_description

        # prep pixel array data
        original_arr_slice = ds.pixel_array
        arr_slice = arr[:, :, -j-1]
        arr_slice[original_arr_slice == -2000 + 1024] = -2000 + 1024
        del ds.PixelData
        ds.PixelData = arr_slice.tobytes()
        ds['PixelData'].VR = 'OW'

        uid = generate_uid(prefix=None)
        ds.file_meta.MediaStorageSOPInstanceUID = uid
        ds.SOPInstanceUID = uid
        ds.SeriesInstanceUID = series_instance_uid
        ds.save_as(Path(output_dicom_path) / original_filename.name)
    print('\n')




# this is the format of paths
#dicom_path = Path("/Users/omerc/Documents/Dataset/test_case/10_friendship_testcase/orig/")
#raw = Path("/Users/omerc/Documents/arineta_final/arineta_cpp/Cpp/CRSIPProcessorRunner/output.rawData")




#this line creates a raw data file from a dicom file
#save_raw_from_dicom(dicom_path,raw)

#this code makes a raw data file from a dicom file
#transform_npy_2_dicom_with_meta(dicom_path,raw,'test_dicom',10,'test')


if __name__ == '__main__':

    mode = 'write'
    raw_data = Path("/Users/omerc/Documents/arineta_final/arineta_cpp/Cpp/arr_3d.rawData")
    raw_data_out = Path("/Users/omerc/Documents/arineta_final/arineta_cpp/Cpp/CRSIPProcessorRunner/output.rawData")
    dicom_path = Path("/Users/omerc/Documents/Dataset/NLM_denoised_data_for_RSIP/1_Friendship_ST_000455/000003_phase_75")
    new_dicom_path = Path("/Users/omerc/Documents/Dataset/NLM_denoised_data_for_RSIP/from_c")

    if mode == 'read': # create a raw data from dicom file
        save_raw_from_dicom(dicom_path, raw_data)
    else: # create a dicom file from raw_data based on original dicom metadata
        transform_npy_2_dicom_with_meta(dicom_path, raw_data_out, new_dicom_path)
