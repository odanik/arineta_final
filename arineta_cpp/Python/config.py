# default parameters

# training parameters:
# added option to auto generate output paths:
generate_path = True
# choose if test or train with model:
mode = 'test'
# path of saved weights for transfer training/testing:
transfer_weights = "../../Python/model_230000iter.ckpt"

# normalization range
norm_range_min =-1024.0
norm_range_max = 3072.0
# reconstruction range
trunc_min = -1024.0
trunc_max = 3072.0

# gpu definitions
device = 'cuda:0'  # gpu to use
num_workers = 1  # how many processors in parallel
