import zmq
import numpy as np
import sys
import time
from solver import Solver

SEND_EXIT    = -10
SEND_NOK     =  50
SEND_OK      =   0
SEND_DENOISE =   1
SEND_TEST    = 100



def log(str):
    print('  [Server] %s' % (str,))


def start_server():
    # Initialize socket
    log('Initializing ZMQ Socket, binding to %s' % sys.argv[1])
    # time.sleep(20)  # Testing connection timeouts
    zmq_context = zmq.Context()
    socket = zmq_context.socket(zmq.REP)
    try:
        socket.bind(sys.argv[1])
    except:
        log("failed socket connection")
        time.sleep(1)
        sys.exit(0)

    # Start recv/send loop
    c = 0
    while True:

        c += 1
        log('recv standby')
        message = socket.recv()
        assert(len(message) >= 1)
        message_type = message[0]
        if message_type > 127:
            message_type = message_type - 256

        # SEND_OK - confirm handshake
        if message_type == SEND_OK:
            time.sleep(1)
            if len(message) != 1:
                log('Bad OK request - message too long (%d)' % len(message))
                socket.send(bytes(chr(SEND_NOK), 'ascii'))
            else:
                socket.send(bytes(chr(SEND_OK), 'ascii'))
                log('python Handshake confirmed')
            continue
        # SEND_EXIT - quit
        elif message_type == SEND_EXIT:
            if len(message) != 1:
                log('Bad EXIT request - message too long (%d)' % len(message))
                socket.send(bytes(chr(SEND_NOK), 'ascii'))
            else:
                socket.send(bytes(chr(SEND_OK), 'ascii'))
                log('Exit request received - exiting')
                time.sleep(1)
                sys.exit(0)
            continue
        # SEND_DENOISE - handle DENOISE request
        elif message_type == SEND_DENOISE:
            try:
                log('Handling DENOISE request')
                index = [1]  # First item already read

                nx = get_int(message, index)
                ny = get_int(message, index)
                nz = get_int(message, index)
                log('nx: {}, ny:{}, nz:{}'.format(nx, ny, nz))

                # process image
                image_data_size = len(message) - index[0]
                assert(image_data_size == 2 * (nx * ny * nz))
                image_buf = np.frombuffer(message[index[0]:len(message)], dtype='int16')
                assert (len(image_buf) == nx * ny * nz)
                log('Read %d ints within range [%f, %f]' % (len(image_buf), min(image_buf), max(image_buf)))
                image_buf = image_buf.reshape((nx, ny, nz))
                new_image_buf = np.copy(image_buf)
                solver = Solver(nz)
                for i in range(nz):
                    slice = solver.test(image_buf[:,:,i], i+1)
                    new_image_buf[:,:,i] = slice

                bytes_image = new_image_buf.tobytes()
                log(" ")

                index[0] = index[0] + image_data_size
                log('Completed message parsing, %d bytes read' % index[0])
                assert(index[0] == len(message))

                # denoise image:

                # Send reply over zqm
                message_size = 1 + 2 * nx * ny * nz
                message = bytearray(message_size)
                message[0] = SEND_OK
                message[1:message_size] = bytes_image

                socket.send(message)
                log('Reply sent')
            except Exception as e:
                log("Inner python bug, exiting child process")
                message_size = 1 + 2 * nx * ny * nz
                message = bytearray(message_size)
                err_len = len(str(e))
                log("length of error:{}".format(len(str(e))))
                err_bytes = str.encode(str(e))
                message[0] = SEND_NOK
                message[1:7] = str.encode(str(err_len).zfill(6))
                message[7:err_len+7] = err_bytes

                socket.send(message)

        elif message_type == SEND_TEST:
            log('Handling TEST request')
            log('Actual buffer size is %d' % len(message))
            index = [1]  # First item already read
            buffer_size = get_int(message, index)
            log('Buffer size read at beginning was %d' % buffer_size)
            index = [len(message) - 4]  # First item already read
            buffer_size = get_int(message, index)
            log('Buffer size read at end was %d' % buffer_size)
            # Send reply over zqm
            socket.send(bytes(chr(SEND_OK), 'ascii'))
            log('Reply sent')



def get_int(message, index):
    index_to = index[0] + 2
    ret = int.from_bytes(message[index[0]:index_to], byteorder='little')

    index[0] = index_to
    return ret


if __name__ == "__main__":
    start_server()
