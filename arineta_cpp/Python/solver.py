import os
import time
import numpy as np
import shutil
from pathlib import Path
import torch
import torchvision
from src.model import EDCNN
import config

class Solver(object):
    def __init__(self, length):
        """

        :param length: (int) number of slices to be processed
        """
        self.length = length
        if config.device:
            self.device = torch.device(config.device)
        else:
            self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.norm_range_min = config.norm_range_min
        self.norm_range_max = config.norm_range_max
        self.trunc_min = config.trunc_min
        self.trunc_max = config.trunc_max
        self.transfer_weights = config.transfer_weights
        self.model = EDCNN()
        self.model.to(self.device)
        self.model.load_state_dict(torch.load(self.transfer_weights, map_location=torch.device(self.device)))
        self.model.eval()



    def denormalize_(self, image):
        """denormalizes prediction
        :param image: model output image
        :return: denormalized prediction
        """
        image = image * (self.norm_range_max - self.norm_range_min) + self.norm_range_min
        return image

    def normalize_(self, image):
        """
        normalization of pixel range
        :param image: np array, image to normalize
        :return: np array, normalized image
        """
        image = (image - self.norm_range_min) / (self.norm_range_max - self.norm_range_min)
        return image

    def trunc(self, mat):
        """
        clip values out of original range
        :param mat: denormalized model output
        :return: model output with values in original range
        """
        mat[mat <= self.trunc_min] = self.trunc_min
        mat[mat >= self.trunc_max] = self.trunc_max
        return mat

    def test(self, x, i):
        """
        predict smooth image with pretrained weights
        :param x: (np_array) unnormalized input slice to process
        :param i: number of slice in series
        :return: unnormalized denoised output
        """
        transform = torchvision.transforms.ToTensor()
        x = transform(self.normalize_(x))
        with torch.no_grad():
                shape_1, shape_2 = x.shape[-2:]
                x = x.unsqueeze(0).float().to(torch.device(self.device))

                pred = self.model(x)

                # denormalize, truncate
                pred = self.trunc(self.denormalize_(pred.view(shape_1, shape_2).cpu().detach()))

                printProgressBar(i, self.length,
                                 prefix="  [Server] Compute predictions ..",
                                 suffix='Complete', length=25)
                pred = np.asarray(pred, dtype='int16')
                return pred


def printProgressBar(iteration, total, prefix='', suffix='', length=100):
    """

    :param iteration: int, current iteration
    :param total: int, number of iterations
    :param prefix: str, string ahead of progress bar
    :param suffix: str, string after of progress bar
    :param length: int, length of progress bar
    :return:
    """
    percent = ("{0:." + str(1) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = '=' * filledLength + ' ' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    if iteration == total:
        print()

