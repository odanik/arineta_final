#include "..\CRSIPProcessor\CRSIPProcessor.h"

#include <fstream>
#include <iostream>
#include <algorithm>

using namespace rsip;

int main(int argc, char* argv[])
{
    std::string patternFilePath = "";

    // Python executable should use backslashes in Windows - otherwise process fails to launch
    std::string pythonPath = "";
    std::string serverPath = "";
    std::string communicationPortString = "";

    if (argc != 7)
    {
        std::cerr << "Usage:" << std::endl
            << "CRSIPProcessorRunner.exe <input-image.rawData> <width> <height> <N_slices> <python-path> <server-path>" << std::endl
            << "  Note: in Windows python-path should use backslashes" << std::endl
            << "Example:" << std::endl
            << "  CRSIPProcessorRunner.exe C:\\Users\\omerc\\Documents\\arineta_final\\arineta_cpp\\Cpp\\arr_3d.rawData "
            << "512 512 280 "
            << "C:\\Users\\omerc\\anaconda3\\envs\\torch\\python.exe "
            << "C:\\Users\\omerc\\Documents\\arineta_final\\arineta_cpp\\Python\\rsip_server.py " << std::endl;
      
        exit(1);
    }
    // def input variables
    patternFilePath = argv[1];
    int width = std::stoi(argv[2]);
    int height = std::stoi(argv[3]);
    int depth = std::stoi(argv[4]);

    // Python executable should use backslashes in Windows - otherwise process fails to launch
    pythonPath = argv[5];
    serverPath = argv[6];
    communicationPortString = "tcp://127.0.0.1:3142";

    // def comand to activate python child process
    std::string dquote = "\"";
    std::string childProcessCommandLine =
        dquote + pythonPath + dquote + " " +
        dquote + serverPath + dquote + " " +
        dquote + communicationPortString + dquote;

    // read raw data file
    std::ifstream file(patternFilePath, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    std::vector<char> result_buffer(size);
    if (!file.read(buffer.data(), size))
    {
        std::cerr << "Could not read file " << patternFilePath << std::endl;
    }
    else 
    {
        std::cerr << "read file " << patternFilePath << std::endl;
    }
    

    RSIPDenoiser processor(
        childProcessCommandLine,
        communicationPortString);
    std::string sError = "No Error!";
    // Check if connection is valid
    bool valid = processor.isReady(sError);
    if (size != 2*width * height * depth) 
    {
        valid = 0;
        sError = "dimention mismatch to array size";
    }
    if (valid == 1) 
    {
        // Call DENOISE
        int output_size = height * width * depth * 2;
        std::vector<char> result(output_size);
        valid = processor.process_DENOISE((int*)buffer.data(), width, height, depth, result, sError);

        if (valid == 1) 
        {
            // write denoised array as rawData
            std::ofstream fout("output.rawData", std::ios::out | std::ios::binary);
            fout.write(&result[0], result.size());
            fout.close();
        }
        
    }
    std::cout << "[Client] Error:" << sError << std::endl;

}
