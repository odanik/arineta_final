from skimage.restoration import denoise_nl_means, estimate_sigma
import os
import time
import numpy as np
from pydicom import dcmread
from pydicom.uid import generate_uid
import shutil
from pathlib import Path
import torch
import torch.optim as optim
from src.model import EDCNN
from src.loss import CompoundLoss
import config

class Solver(object):
    def __init__(self, args, data_loader):
        """
        main train and test model
        :param args: command-line parsing module
        :param data_loader: pre-defined dataloader
        """
        self.mode = args.mode
        self.data_loader = data_loader
        self.dicom_path = args.dicom_path
        self.apply_nlm = args.apply_nlm
        if args.device:
            self.device = torch.device(args.device)
        else:
            self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.norm_range_min = config.norm_range_min
        self.norm_range_max = config.norm_range_max
        self.trunc_min = config.trunc_min
        self.trunc_max = config.trunc_max

        self.save_path = args.save_path
        self.init_with_transfer = args.init_with_transfer
        self.transfer_weights = args.transfer_weights

        self.num_epochs = config.num_epochs
        self.print_iters = config.print_iters
        self.decay_iters = config.decay_iters
        self.save_iters = config.save_iters
        self.test_iters = args.test_iters
        self.test_patient = args.test_patient
        self.series_description = args.series_description

        self.patch_size = config.patch_size
        self.model = EDCNN()
        self.model.to(self.device)

        self.lr = config.lr
        self.criterion = CompoundLoss()
        self.criterion.to(self.device)
        self.optimizer = optim.Adam(self.model.parameters(), self.lr)


    def save_model(self, iter_):
        """
        save trained model weights
        :param iter_: saving iteration
        """
        f = os.path.join(self.save_path, 'model_{}iter.ckpt'.format(iter_))
        torch.save(self.model.state_dict(), f)


    def load_model(self):
        """
        load pretrained model weights
        """
        self.model.load_state_dict(torch.load(self.transfer_weights, map_location=torch.device(self.device)))


    def lr_decay(self):
        """
        reduces learning rate
        """
        lr = self.lr * 0.5
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = lr


    def denormalize_(self, image):
        """denormalizes prediction
        :param image: model output image
        :return: denormalized prediction
        """
        image = image * (self.norm_range_max - self.norm_range_min) + self.norm_range_min
        return image


    def trunc(self, mat):
        """
        clip values out of original range
        :param mat: denormalized model output
        :return: model output with values in original range
        """
        mat[mat <= self.trunc_min] = self.trunc_min
        mat[mat >= self.trunc_max] = self.trunc_max
        return mat


    def save_npy(self, pred, fig_name):
        """
        saves prediction as numpy array
        :param pred: array to save
        :param fig_name: saving name of array
        """
        npy_save_path = os.path.join(self.save_path, 'npy', str(self.test_iters), self.test_patient,
                                     '{}'.format(str(fig_name+1).zfill(5)))
        np.save(npy_save_path, pred.numpy())


    def train(self):
        """
        perform training session
        """
        if self.init_with_transfer:
            self.load_model(self.test_iters)
        train_losses = []
        total_iters = 0
        start_time = time.time()
        for epoch in range(1, self.num_epochs):
            self.model.train(True)

            for iter_, (x, y, asir_nlm) in enumerate(self.data_loader):
                total_iters += 1
                # add 1 channel
                x = x.unsqueeze(0).float().to(self.device)
                y = y.unsqueeze(0).float().to(self.device)
                asir_nlm = asir_nlm.unsqueeze(0).float().to(self.device)

                if self.patch_size: # patch training
                    x = x.view(-1, 1, self.patch_size, self.patch_size)
                    y = y.view(-1, 1, self.patch_size, self.patch_size)
                    asir_nlm = asir_nlm.view(-1, 1, self.patch_size, self.patch_size)

                # forward step
                pred = self.model(x)
                loss = self.criterion(pred, asir_nlm, y)
                self.model.zero_grad()
                self.optimizer.zero_grad()

                # backward step
                loss.backward()
                self.optimizer.step()
                train_losses.append(loss.item())

                # print
                if total_iters % self.print_iters == 0:
                    print("STEP [{}], EPOCH [{}/{}], ITER [{}/{}] \nLOSS: {:.8f}, TIME: {:.1f}s".format(total_iters, epoch,
                                                                                                        self.num_epochs, iter_+1,
                                                                                                        len(self.data_loader), loss.item(),
                                                                                                        time.time() - start_time))
                # learning rate decay
                if total_iters % self.decay_iters == 0:
                    self.lr_decay()
                # save model
                if total_iters % self.save_iters == 0:
                    self.save_model(total_iters)
                    np.save(os.path.join(self.save_path, 'loss_{}_iter.npy'.format(total_iters)), np.array(train_losses))

    def train_double(self):
        """
        perform training session
        """
        if self.init_with_transfer:
            self.load_model(self.test_iters)
        train_losses = []
        total_iters = 0
        start_time = time.time()
        for epoch in range(1, self.num_epochs):
            self.model.train(True)

            for iter_, (x, x2, y, asir_nlm) in enumerate(self.data_loader):
                total_iters += 1
                # add 1 channel
                x = x.unsqueeze(0).float().to(self.device)
                x2 = x2.unsqueeze(0).float().to(self.device)
                y = y.unsqueeze(0).float().to(self.device)
                asir_nlm = asir_nlm.unsqueeze(0).float().to(self.device)

                if self.patch_size:  # patch training
                    x = x.view(-1, 1, self.patch_size, self.patch_size)
                    x2 = x2.view(-1, 1, self.patch_size, self.patch_size)
                    y = y.view(-1, 1, self.patch_size, self.patch_size)
                    asir_nlm = asir_nlm.view(-1, 1, self.patch_size, self.patch_size)

                # forward step
                pred = self.model(x, x2)
                loss = self.criterion(pred, asir_nlm, y)
                self.model.zero_grad()
                self.optimizer.zero_grad()

                # backward step
                loss.backward()
                self.optimizer.step()
                train_losses.append(loss.item())

                # print
                if total_iters % self.print_iters == 0:
                    print("STEP [{}], EPOCH [{}/{}], ITER [{}/{}] \nLOSS: {:.8f}, TIME: {:.1f}s".format(total_iters,
                                                                                                        epoch,
                                                                                                        self.num_epochs,
                                                                                                        iter_ + 1,
                                                                                                        len(self.data_loader),
                                                                                                        loss.item(),
                                                                                                        time.time() - start_time))
                # learning rate decay
                if total_iters % self.decay_iters == 0:
                    self.lr_decay()
                # save model
                if total_iters % self.save_iters == 0:
                    self.save_model(total_iters)
                    np.save(os.path.join(self.save_path, 'loss_{}_iter.npy'.format(total_iters)),
                            np.array(train_losses))

    def test(self):
        """
        predict smooth image with pretrained weights
        """
        del self.model
        # load
        self.model = EDCNN().to(self.device)
        self.load_model()

        self.model.eval()
        with torch.no_grad():
            for i, x in enumerate(self.data_loader):
                shape_1, shape_2 = x.shape[-2:]
                x = x.unsqueeze(0).float().to(self.device)

                pred = self.model(x)

                # denormalize, truncate
                pred = self.trunc(self.denormalize_(pred.view(shape_1, shape_2).cpu().detach()))

                # save result NPY
                self.save_npy(pred, i)

                printProgressBar(i, len(self.data_loader),
                                 prefix="Compute predictions ..",
                                 suffix='Complete', length=25)

        original_dicom_path = self.dicom_path
        npy_path = Path(self.save_path) / 'npy' / str(self.test_iters) / str(
            self.test_patient)

        output_dicom_path = Path(self.save_path) / 'dicom_prediction' / (str(
            self.test_patient) + '_prediction_iter_' + str(self.test_iters))

        transform_2dnpy_2_dicom_with_meta(original_dicom_path, npy_path, output_dicom_path, series_number_add=44,
                                               description=self.series_description, apply_nlm_bool=self.apply_nlm)

    def test_double(self):
        """
        predict smooth image with pretrained weights
        """
        del self.model
        # load
        self.model = EDCNN().to(self.device)
        self.load_model()

        self.model.eval()
        with torch.no_grad():
            for i, (x, x2) in enumerate(self.data_loader):
                shape_1, shape_2 = x.shape[-2:]
                x = x.unsqueeze(0).float().to(self.device)
                x2 = x2.unsqueeze(0).float().to(self.device)

                pred = self.model(x, x2)

                # denormalize, truncate
                pred = self.trunc(self.denormalize_(pred.view(shape_1, shape_2).cpu().detach()))

                # save result NPY
                self.save_npy(pred, i)

                printProgressBar(i, len(self.data_loader),
                                 prefix="Compute predictions ..",
                                 suffix='Complete', length=25)

        original_dicom_path = self.dicom_path
        npy_path = Path(self.save_path) / 'npy' / str(self.test_iters) / str(
            self.test_patient)

        output_dicom_path = Path(self.save_path) / 'dicom_prediction' / (str(
            self.test_patient) + '_prediction_iter_' + str(self.test_iters))

        transform_2dnpy_2_dicom_with_meta(original_dicom_path, npy_path, output_dicom_path, series_number_add=44,
                                               description=self.series_description, apply_nlm_bool=self.apply_nlm)

def transform_2dnpy_2_dicom_with_meta(original_dicom_path, npy_path, output_dicom_path, series_number_add,
                                      description, apply_nlm_bool, load=True):
    """
    transform np data to dicom using a specific dicom file metadata
    :param original_dicom_path: str, path of original dicom folder to take metadata from
    :param npy_path: str, path of folder containing np data to transform
    :param output_dicom_path: str, path to save created dicom files
    :param series_number_add: int, random number to add to avoid collision
    :param description: str, description of new dicom file
    :param apply_nlm: bool, if True, apply nlm before saving
    :return: saves dicom file in dir
    """
    # create path
    if output_dicom_path.is_dir():
        shutil.rmtree(output_dicom_path)
    output_dicom_path.mkdir(exist_ok=True, parents=True)

    if load:
        # load array
        file_paths = sorted(Path(npy_path).glob('*.npy'))
        npy_data_list = []
        print('\n')
        N = len(file_paths)
        for j, npy_file_path in enumerate(file_paths):
            print('loading file {}/{}'.format(j+1, N),end = "\r")
            np_data = np.load(npy_file_path).astype(np.int16)
            npy_data_list.append(np_data.astype(np.int16))
        arr = np.stack(npy_data_list, axis=2) + 1024
        print('\n')
    else:
        arr = npy_path+1024
        N = arr.shape[2]
    # smooth prediction with nlm:
    if apply_nlm_bool:
        arr = apply_nlm(arr)
        print('\n')
    # process dicom
    series_instance_uid = generate_uid(prefix=None)

    for j, original_filename in enumerate(sorted(Path(original_dicom_path).glob('*'))):
        print('processing file {}/{}'.format(j + 1, N), end="\r")

        # prep metadata
        ds = dcmread(original_filename)
        ds.SeriesNumber = str(int(ds.SeriesNumber + series_number_add))
        ds_description_list = ds.SeriesDescription.split(',')
        ds_description_list.insert(2, description)
        ds_description = ','.join(ds_description_list)
        ds.SeriesDescription = ds_description

        # prep pixel array data
        original_arr_slice = ds.pixel_array
        arr_slice = arr[:, :, -j-1]
        arr_slice[original_arr_slice == -2000 + 1024] = -2000 + 1024
        del ds.PixelData
        ds.PixelData = arr_slice.tobytes()
        ds['PixelData'].VR = 'OW'

        uid = generate_uid(prefix=None)
        ds.file_meta.MediaStorageSOPInstanceUID = uid
        ds.SOPInstanceUID = uid
        ds.SeriesInstanceUID = series_instance_uid
        ds.save_as(Path(output_dicom_path) / original_filename.name)
    print('\n')


def apply_nlm(arr, sigma_factor=10, fast=True):
    """
    apply non-local means (NLM) denoising to a CT
    :param arr: Numpy array representing the CT
    :param sigma_factor: float. the factor to multiply sigma noise to get smoothness scale
    :param slices: range. Restrict the denoising to a specific slice range
    :param fast: Whether to run NLM in fast mode (True) or not (False)
    :return: Numpy array with NLM applied
    """
    arr = arr.copy(order='C')
    patch_kw = dict(patch_size=5,       # 5x5 patches
                patch_distance=21,       # 21x21 search area
                multichannel=False)
    sigma_est = estimate_sigma(arr)
    start = time.perf_counter()

    denoise = np.zeros_like(arr)
    slices = range(arr.shape[2])
    for i in slices:
        print('applying NLM on slice {}/{}'.format(i, arr.shape[2]), end='\r')
        denoise[:,:,i] = denoise_nl_means(arr[:,:,i], h = sigma_factor * sigma_est,
                                          fast_mode = fast, **patch_kw, preserve_range=True)
    end = time.perf_counter()
    print('\n', f'Run time = {(end - start):0.1f}')
    return denoise


def printProgressBar(iteration, total, prefix='', suffix='', length=100):
    """

    :param iteration: int, current iteration
    :param total: int, number of iterations
    :param prefix: str, string ahead of progress bar
    :param suffix: str, string after of progress bar
    :param length: int, length of progress bar
    :return:
    """
    percent = ("{0:." + str(1) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = '=' * filledLength + ' ' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    if iteration == total:
        print()


